Authors
=======
We'd like to thank the following people for their contributions:


- Andreas Redmer \<andreasredmer⒜mailchuck∙com\>
- Dmitry Mikhirev \<mikhirev⒜gmail∙com\>
- vagrant \<vagrant⒜vagrant∙vm\>
- Verdulo \<cybertomek⒜openmailbox∙org\>
- xin \<xin⒜riseup∙net\>
- Сухичев Михаил Иванович \<sukhichev⒜yandex∙ru\>
