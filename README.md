# Apple Flinger

[![build status](https://gitlab.com/ar-/apple-flinger/badges/master/build.svg)](https://gitlab.com/ar-/apple-flinger/builds)

*Funny single- and multiplayer game for Android - Use a slingshot to shoot with apples. Very challenging and addictive.*

![screenshot](https://gitlab.com/ar-/apple-flinger/raw/master/metadata/en-AU/images/featureGraphic.png)

Features:

*    high resolution quality graphics
*    realistic physics
*    lag-free and smooth gameplay
*    fine detailed animations and particle systems
*    brand new and innovative game concept
*    single/multiplayer
*    100% open source (GPL3)

You use a slingshot to shoot with apples. Be first to destroy the whole enemy base, but be aware, the other side shoots back. This game has a balanced mix of puzzle, strategy, patience and action.

This is for one or two players! You can play this against anyone who sits next to you - or against yourself. You can also play against a computer controlled opponent. More features, more fun and more levels will come soon.

international age ratings:

*    ACB: G (general)
*    ClassInd: L
*    ESRB: E (everyone)
*    PEGI:3
*    USK: 0
*    IARC: 3

## Quickstart

Option 1: [![Get it on F-Droid](https://f-droid.org/wiki/images/3/31/F-Droid-button_get-it-on_smaller.png)](https://f-droid.org/packages/com.gitlab.ardash.appleflinger.android/)

Option 2: Download the latest release or an older release from
https://gitlab.com/ar-/apple-flinger/tags

Option 3: Download the latest stable build from gitlab's master branch.
https://gitlab.com/ar-/apple-flinger/-/jobs/artifacts/master/download?job=build_release

Option 4: Build it from source

	git clone https://gitlab.com/ar-/apple-flinger.git
	cd apple-flinger
	./gradlew assembleRelease

## Contributors

See the [AUTHORS](AUTHORS.md) file for a list of all contributors.

All merge requests are welcome. Contributions can be made towards:
Programming, Testing, Graphic Design, Web page, Sound engineering, Level design, Documentation, Consulting, ...

Send an email to incoming+ar-/apple-flinger@gitlab.com if you need help with anything.


